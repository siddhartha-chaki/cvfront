import React from "react";
import { NavLink } from "react-router-dom";

const Home = () => {
  return (
    <>
      <div className="login-form">
        <form method="POST">
          <h2 className="text-center">Recuters's LAB</h2>
          <div className="form-group">
          <div class="input-group mb-3">
            <input type="file" class="form-control" id="inputGroupFile02" required="required" multiple/>
          
            <span className="input-group-text" for="inputGroupFile02">
                  <span className="fa fa-user"></span>
                </span>
            
          </div>
          </div>
          
          
            <div className="form-group">
            <div class="input-group">
            <div className="input-group-prepend">
                <span className="input-group-text">
                  <i className="fa fa-lock"></i>
                </span>
              </div>
              <select class="form-select form-select-sm" aria-label=".form-select-sm example">
              <option selected>Positions</option>
              <option value="1">Analyst 2+</option>
              <option value="2">Associate 5+</option>
              <option value="3">Vice President 10+</option>
            </select>
            <select class="form-select form-select-sm" aria-label=".form-select-sm example">
              <option selected>Department</option>
              <option value="1">Technology</option>
              <option value="2">Business</option>
              <option value="3">Human Resource</option>
            </select>
            <input type="file" class="form-control" id="inputGroupFile03" required="required" />

        </div>
          </div>
          <div className="form-group">
            <button
              type="submit"
              className="btn btn-primary login-btn btn-block"
              name="signin"
              value="Signin"
              // onClick={loginUser}
            >
              Get Results
            </button>
          </div>
          
          <div className="or-seperator">
            
            <i>History</i>
          </div>
          <p className="text-center">Login with your social media account</p>
          <div className="text-center social-btn">
            <NavLink to="/" className="btn btn-secondary">
              <i className="fa fa-facebook"></i>&nbsp; Facebook
            </NavLink>
            <NavLink to="/" className="btn btn-info">
              <i className="fa fa-twitter"></i>&nbsp; Twitter
            </NavLink>
            <NavLink to="/" className="btn btn-danger">
              <i className="fa fa-google"></i>&nbsp; Google
            </NavLink>
          </div>
        </form>
        <p className="text-center text-muted small">
          Don't have an account? <NavLink to="/signup">Sign up here!</NavLink>
        </p>
      </div>
    </>
  );
};

export default Home;
