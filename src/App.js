import React from 'react'
import Navbar from './componets/Navbar'
import Home from './componets/Home'
import About from './componets/About'
import Resumes from './componets/Resumes'
import JDs from './componets/JDs'
import { Route, Routes } from 'react-router-dom'
import './App.css'

const App = () => {
  return (
    <div>
      <Navbar/>
      <Routes>
      <Route path="/" exact element={<Home />} />
        <Route path="/resume" element={<Resumes />} />
        <Route path="/jds" element={<JDs />} />
        <Route path="/about" element={<About />} />
    </Routes>
    </div>
    
  )
}

export default App